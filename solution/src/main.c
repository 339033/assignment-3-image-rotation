

#include "image.h"
#include "bmp.h"
#include "rotate.h"
#include "util.h"
#include <string.h>
int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning


    struct image originalImage;
    struct image rotatedImage;


    if(argc < 4){
        printErr(INCRCT_NUMBER);
        return -1;
    }

    const char* extension = file_extension(argv[1]);

    enum read_status readStat;
    enum read_statusG24 readStatG;
    if (strcmp(extension, "bmp") == 0) {
        readStat = readStatus_from_bmp(argv[1], &originalImage);
        if (readStat != READ_OK) {
            printErr(INVALID_INPUT);
        }
    } else if (strcmp(extension, "g24") == 0) {
        readStatG = readStatus_from_g24(argv[1], &originalImage);
        if (readStatG != READG24_OK) {
            printErr(INVALID_INPUT);
        }
    } else {
        printErr(INVALID_INPUT);
        return -1;
    }





    print_newline();
    char *endptr;
    long angle = strtol(argv[3], &endptr, 10);
    if(*endptr=='\0'){
        if (angle == 180 || angle == 0 || angle == -180 || angle == 90 || angle == -90 || angle == -270 || angle == 270)
            rotatedImage = rotate(originalImage, angle);
        else {
            printErr(INCORR_ANGLE);
            return -1;
        }
    }

    else{
        printErr(INVALID_INPUT);
        return -1;
    }

    if(angle!=0){
        delete_image(&originalImage);
    }





    enum write_status writeStat __attribute__((unused));
    enum write_statusG24 writeGstat;

    // Проверка расширения файла и вызов соответствующей функции записи
    if (strcmp(file_extension(argv[2]), ".g24") == 0) {
        writeGstat = WRITEG24_NOT_APPLICABLE;  // Значение по умолчанию
        writeStat = writeStatus_to_g24(argv[2], &rotatedImage, &writeGstat);
        if (writeGstat != WRITEG24_OK) {
            printErr(ERR_OUTPUT);
        }
        delete_image(&rotatedImage);
    } else {
        writeStat = writeStatus_to_bmp(argv[2], &rotatedImage);
        if (writeStat != WRITE_OK) {
            printErr(ERR_OUTPUT);
        }
        delete_image(&rotatedImage);
    }



    return 0;


}

