#include "image.h"
#include <malloc.h>
#include <stdio.h>

//
// Created by 79819 on 23.11.2023.
//


struct image create_new_image(const uint64_t w, const uint64_t h) {
    return (struct image) {
            .width = w,
            .height = h,
            .data = (struct pixel*) malloc(sizeof(struct pixel) * w * h)
    };
}

void delete_image(struct image* img) {
    if (img) {
        free(img->data);
        img->data = NULL;
    }
}
void malloc_check(void *ptr) {
    if (ptr == NULL) {
        fprintf(stderr, "Out of memory error");
        exit(69);
    }
}

struct pixel* malloc_pixels(size_t width, size_t height) {
    struct pixel* pixels = (struct pixel*) malloc(sizeof(struct pixel) * width * height);
    malloc_check(pixels);
    return pixels;
}
