
#include "rotate.h"
#include <stdbool.h>
static bool check(struct image const source, struct image const rotated_img) {
    if (!source.data || !rotated_img.data) return false;
    return true;
}
struct image rotation90 (struct image rotated_image, uint64_t w, uint64_t h, struct pixel *data){

    for (size_t i = 0; i < h; i++) {
        for (size_t j = 0; j < w; j++) {
            size_t source_index = j + i * w;
            size_t rotated_index = (h - 1 - i) + j * h;
            rotated_image.data[rotated_index] = data[source_index];
        }
    }
    return rotated_image;
}
struct image rotation180 (struct image rotated_image, uint64_t w, uint64_t h,struct pixel *data){


    for (size_t i = 0; i < h; i++) {
        for (size_t j = 0; j < w; j++) {
            size_t source_index = j + i * w;
            size_t rotated_index = (h - 1 - i) * w + (w - 1 - j);
            rotated_image.data[rotated_index] = data[source_index];
        }
    }
    return rotated_image;
}
struct image rotation270 (struct image rotated_image, uint64_t w, uint64_t h, struct pixel *data){

    for (size_t i = 0; i < h; i++) {
        for (size_t j = 0; j < w; j++) {
            size_t source_index = j + i * w;
            size_t rotated_index = (w - 1 - j) * h + i;
            rotated_image.data[rotated_index] = data[source_index];
        }
    }
    return rotated_image;
}
struct image rotate( struct  image const source, long angle){


    switch (angle) {
        case 0: {
            return source;
        }
        case 90: case -270: {
            struct image rotated_image = create_new_image(source.height, source.width);
            return rotation90(rotated_image,source.width,source.height,source.data);

        }
        case 180: case -180: {
            struct image rotated_image = create_new_image(source.width, source.height);
            if (!check(source, rotated_image)) return (struct image) {0};

            return rotation180(rotated_image, source.width, source.height,source.data);
        }
        case 270: case -90: {
            struct image rotated_image = create_new_image(source.height, source.width);
            if (!check(source, rotated_image)) return (struct image) {0};

            return rotation270(rotated_image, source.width, source.height, source.data);
        }

    }


    return source;
}



