#include "util.h"
#include <stdio.h>
#include <string.h>


char* error_map[] = {
        [INCORR_ANGLE] = "Error: Incorrect angle, it should be (0, 90, -90, 180, -180, 270, -270)!",
        [INCRCT_NUMBER]="Incorrect numbers= of elements",
        [ERR_INPUT]="Error with reading input file",
        [ERR_OUTPUT]="Error with writting output file",
        [ERR_CLOSING_OUTPUT]="Error closing the input file",
        [INVALID_INPUT]="Error: Invalid input, it should be an integer!"

};


uint16_t my_htons(uint16_t value) {
    return ((value & 0xFF) << 8) | ((value & 0xFF00) >> 8);
}
void printErr(enum Errors const err){

        fprintf(stderr, "%s", error_map[err]);


}
void print_newline(void) {
    printf("\n");
}

const char* file_extension(const char* filename) {
    const char* dot = strrchr(filename, '.');
    if (!dot || dot == filename) {
        return "";
    }
    return dot + 1;
}

uint16_t read_uint16_t(FILE* file) {
    uint16_t value;
    fread(&value, sizeof(uint16_t), 1, file);
    return value;
}


