



#include "bmp.h"
#include "util.h"
#include <string.h>

long get_padding(uint32_t const width){
    return (int32_t) (4 - (width * sizeof(struct pixel)) % 4) % 4;
}

enum read_status from_bmp(FILE* in, struct image* img) {
    if (!in) {
        return READ_INVALID_FILE;
    }

    struct bmp_header header;
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }

    if (header.bfType != 0x4d42) {
        printf("%hu", header.bfType);
        return READ_INVALID_SIGNATURE;
    }

    if (header.biBitCount != 24) {
        return READ_INVALID_BITS;
    }

    if (header.biCompression != 0) {
        return READ_INVALID_COMPRESSION;
    }

    *img = create_new_image((uint64_t)header.biWidth, (uint64_t)header.biHeight);
    if (!img->data) {
        return READ_IMG_IS_NULL;
    }
    if (header.bfType == 0x4732) {
        fseek(in, 4, SEEK_SET); // Пропустить 4 байта (ширина и высота)
        img->width = read_uint16_t(in);
        img->height = read_uint16_t(in);
        img->data = (struct pixel*)malloc(img->width * img->height * sizeof(struct pixel));
        if (!img->data) {
            return READ_IMG_IS_NULL;
        }
        if (fread(img->data, sizeof(struct pixel), img->width * img->height, in) != img->width * img->height) {
            return READ_IMAGE_ERROR;
        }
        return READ_G24_OK;
    }
    fseek(in, (long)header.bOffBits, SEEK_SET);
    long padding = get_padding(img->width);

    for (size_t i = 0; i < img->height; i++) {
        size_t reversedIndex = img->height - i - 1;
        if (fread(&img->data[reversedIndex * img->width], sizeof(struct pixel), img->width, in) != img->width) {
            return READ_IMAGE_ERROR;
        }
        fseek(in, padding, SEEK_CUR);
    }

    return READ_OK;
}
static struct bmp_header create_header(const struct image *img) {
    long padding = get_padding(img->width);
    return (struct bmp_header) {
            .bfType = 0x4d42,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = (img->width)*(img->height) + get_padding(img->width)*(img->height),
            .bfileSize = img->height * (img->width * sizeof(struct pixel) + padding),
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
}
void my_copy(void *dest, const void *src, size_t size) {
    char *d = (char *)dest;
    const char *s = (const char *)src;

    for (size_t i = 0; i < size; ++i) {
        d[i] = s[i];
    }
}

// Внутренняя функция для инверсии строк
static void invert_rows_g24(struct pixel* data, uint64_t width, uint64_t height) {
    struct pixel* temp_row = malloc(width * sizeof(struct pixel));
    if (!temp_row) {
        // Обработка ошибки выделения памяти
        return;
    }

    size_t row_size = width * sizeof(struct pixel);

    for (uint64_t i = 0; i < height / 2; ++i) {
        // Используем свою функцию копирования данных
        my_copy(temp_row, &data[i * width], row_size);
        my_copy(&data[i * width], &data[(height - i - 1) * width], row_size);
        my_copy(&data[(height - i - 1) * width], temp_row, row_size);
    }

    free(temp_row);
}
enum write_status to_bmp(FILE* out, const char* filename, struct image const* img) {
    long padding = get_padding(img->width);
    struct bmp_header header = create_header(img);

    // Проверка, нужно ли записывать в G24 формат
    if (strcmp(file_extension(filename), ".g24") == 0) {
        fwrite(&img->width, sizeof(uint16_t), 1, out);
        fwrite(&img->height, sizeof(uint16_t), 1, out);
        fwrite(img->data, sizeof(struct pixel), img->width * img->height, out);
        return WRITE_G24_OK;
    }

    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
        return WRITE_HEADER_ERROR;
    }

    for (size_t i = 0; i < img->height; i++) {
        size_t reversedIndex = img->height - i - 1;
        if (fwrite(&img->data[reversedIndex * img->width], sizeof(struct pixel), img->width, out) != img->width) {
            return WRITE_BIT_ERROR;
        }
        for (size_t j = 0; j < padding; j++) {
            fputc(0, out);
        }
    }

    return WRITE_OK;
}
enum read_statusG24 readStatus_from_g24(const char* filename, struct image* img) {
    FILE* file = fopen(filename, "rb");
    if (!file) {
        return READG24_INVALID_FILE;
    }

    // Считываем ширину и высоту
    uint16_t width, height;
    if (fread(&width, sizeof(uint16_t), 1, file) != 1 || fread(&height, sizeof(uint16_t), 1, file) != 1) {
        fclose(file);
        return READG24_INVALID_FORMAT;
    }

    img->width = (uint64_t)width;
    img->height = (uint64_t)height;

    // Выделяем память под изображение
    img->data = (struct pixel*)malloc(img->width * img->height * sizeof(struct pixel));
    if (!img->data) {
        fclose(file);
        return READG24_IMG_IS_NULL;
    }

    // Считываем пиксели
    if (fread(img->data, sizeof(struct pixel), img->width * img->height, file) != img->width * img->height) {
        free(img->data);
        fclose(file);
        return READG24_IMAGE_ERROR;
    }

    invert_rows_g24(img->data, img->width, img->height);
    fclose(file);
    return READG24_OK;
}
enum write_status writeStatus_to_g24(const char* filename, struct image const* img, enum write_statusG24* g24Status) {
    FILE* file = fopen(filename, "wb");
    if (!file) {
        return WRITE_HEADER_ERROR;
    }

    // Проверка, нужно ли записывать в G24 формат

        // Запись ширины и высоты
        uint16_t width = my_htons(img->width);  // Конвертируем в сетевой порядок байт
        uint16_t height = my_htons(img->height); // Конвертируем в сетевой порядок байт
        fwrite(&width, sizeof(uint16_t), 1, file);
        fwrite(&height, sizeof(uint16_t), 1, file);

        // Запись пикселей
        invert_rows_g24(img->data, img->width, img->height);
        fwrite(img->data, sizeof(struct pixel), img->width * img->height, file);

        fclose(file);
        *g24Status = WRITEG24_OK;
        return WRITE_G24_OK;


    // В случае, если расширение файла не .g24, вызываем to_bmp
    //enum write_status status = to_bmp(file, filename, img);
    fclose(file);
    *g24Status = WRITEG24_NOT_APPLICABLE;

}





enum read_status readStatus_from_bmp(const char* filename, struct image* img){
    FILE* file = fopen(filename, "rb");
    if (!file){
        return READ_INVALID_FILE;
    }
    enum read_status status = from_bmp(file, img);
    fclose(file);
    return status;
}
enum write_status writeStatus_to_bmp(const char* filename, struct image const* img) {
    FILE* file = fopen(filename, "wb");
    if (!file) {
        return WRITE_HEADER_ERROR;
    }

    enum write_status status = to_bmp(file, filename, img);
    fclose(file);
    return status;
}












