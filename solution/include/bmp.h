//
// Created by 79819 on 22.11.2023.
//

#ifndef IMAGE_TRANSFORMER_BMP_H
#define IMAGE_TRANSFORMER_BMP_H

#include "image.h"
#include <stdint.h>
#include <stdio.h>
#pragma pack(push, 1)

struct bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

#pragma pack(pop)


enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_FILE,
    READ_IMAGE_ERROR,
    READ_IMG_IS_NULL,
    READ_INVALID_COMPRESSION,
    READ_G24_OK
};



enum read_status from_bmp( FILE* in, struct image* img );
enum  write_status  {
    WRITE_OK = 0,
    WRITE_HEADER_ERROR,
    WRITE_BIT_ERROR,
    WRITE_G24_OK
};
enum read_statusG24 {
    READG24_OK = 0,
    READG24_INVALID_FILE,
    READG24_INVALID_FORMAT,
    READG24_IMG_IS_NULL,
    READG24_IMAGE_ERROR

};
enum write_statusG24 {
    WRITEG24_OK = 0,
    WRITEG24_NOT_APPLICABLE
};



enum read_statusG24 readStatus_from_g24(const char* filename, struct image* img);
enum write_status to_bmp( FILE* out, const char* filename, struct image const* img );
enum write_status writeStatus_to_g24(const char* filename, struct image const* img, enum write_statusG24* g24Status);

long get_padding(uint32_t const width);

enum read_status readStatus_from_bmp(const char* filename, struct image* img);
enum write_status writeStatus_to_bmp(const char* filename, struct image const* img);









#endif //IMAGE_TRANSFORMER_BMP_H
