//
// Created by 79819 on 23.11.2023.
//

#ifndef IMAGE_TRANSFORMER_UTIL_H
#define IMAGE_TRANSFORMER_UTIL_H

#include <stdint.h>
#include <stdio.h>
uint16_t my_htons(uint16_t value);
enum Errors{
    INCRCT_NUMBER,
    ERR_INPUT,
    ERR_OUTPUT,
    ERR_CLOSING_OUTPUT,
    INCORR_ANGLE,
    INVALID_INPUT
};
void printErr(enum Errors const err);

extern char* error_map[];
void print_newline(void);
const char* file_extension(const char* filename);
uint16_t read_uint16_t(FILE* file);
#endif //IMAGE_TRANSFORMER_UTIL_H
