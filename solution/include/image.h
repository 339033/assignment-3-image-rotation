
#ifndef ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_H
#include <inttypes.h>
#include <stdlib.h>
#pragma pack(push, 1)
struct pixel { uint8_t b, g, r; };
#pragma pack(pop)
struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image create_new_image(uint64_t width, uint64_t height);

void delete_image(struct image* img);




struct pixel* malloc_pixels(size_t width, size_t height);
void malloc_check(void *ptr);
#endif //ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_H
