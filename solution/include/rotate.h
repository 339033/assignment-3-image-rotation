//
// Created by 79819 on 23.11.2023.
//

#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H
#include "image.h"
struct image rotate(struct image const source, long angle);
#endif //IMAGE_TRANSFORMER_ROTATE_H
